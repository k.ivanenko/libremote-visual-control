#ifndef IPROTOCOL_H
#define IPROTOCOL_H

#include <cstdint>
#include <string>
#include <functional>

namespace RVisCtl {

class IProtocol {
public:
    virtual ~IProtocol() = default;
    using ConnectingCallback = std::function<void ()>;

    virtual void connect(const std::string& host, uint16_t port) = 0;
    virtual void disconnect() = 0;

    virtual bool isConnected() = 0;

    void setConnectedCallback(const ConnectingCallback& callback) { m_connectedCallback = callback;};
    void setDisconnectedCallback(const ConnectingCallback& callback) {m_disconnectedCallback =  callback; };

    virtual void cursorMoveTo(int x, int y) = 0;
    virtual void keyPress(uint32_t keysum, bool isPress) = 0;

protected:
    ConnectingCallback m_connectedCallback;
    ConnectingCallback m_disconnectedCallback;
};

}

#endif // IPROTOCOL_H
