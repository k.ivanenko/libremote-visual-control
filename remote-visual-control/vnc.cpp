#include "vnc.h"

#include <iostream>
#include <map>

namespace RVisCtl {

std::map<rfbClient*, VNC*> VNC::m_instances;

void VNC::handleTextChatProc(rfbClient* c, int value, char *text)
{
    auto obj = VNC::instance(c);
    if (obj && obj->m_handleTextChatCallback) obj->m_handleTextChatCallback(value, text);
}

void VNC::handleXvpMsgProc(rfbClient* c, uint8_t version, uint8_t opcode)
{
    auto obj = VNC::instance(c);
    if (obj && obj->m_handleXvpMsgCallback) obj->m_handleXvpMsgCallback(version, opcode);
}

void VNC::handleKeyboardLedStateProc(rfbClient* c, int value, int pad)
{
    auto obj = VNC::instance(c);
    if (obj && obj->m_handleKeyboardLedStateCallback) obj->m_handleKeyboardLedStateCallback(value, pad);
}

rfbBool VNC::handleCursorPosProc(rfbClient* c, int x, int y)
{
    auto obj = VNC::instance(c);
    if (obj && obj->m_handleCursorPosCallback) return obj->m_handleCursorPosCallback(x, y);
    return false;
}

void VNC::softCursorLockAreaProc(rfbClient* c, int x, int y, int w, int h)
{
    auto obj = VNC::instance(c);
    if (obj && obj->m_softCursorLockAreaCallback) obj->m_softCursorLockAreaCallback(x, y, w, h);
}

void VNC::softCursorUnlockScreenProc(rfbClient* c)
{
    auto obj = VNC::instance(c);
    if (obj && obj->m_softCursorUnlockScreenCallback) obj->m_softCursorUnlockScreenCallback();
}

void VNC::gotFrameBufferUpdateProc(rfbClient* c, int x, int y, int w, int h)
{
    auto obj = VNC::instance(c);
    if (obj && obj->m_gotFrameBufferUpdateCallback)
        obj->m_gotFrameBufferUpdateCallback(x, y, w, h);
}

void VNC::finishedFrameBufferUpdateProc (rfbClient* c)
{
    auto obj = VNC::instance(c);
    if (obj && obj->m_finishedFrameBufferUpdateCallback) obj->m_finishedFrameBufferUpdateCallback();
}

char* VNC::getPasswordProc(rfbClient* c)
{
    auto obj = VNC::instance(c);
    if (obj && obj->m_getPasswordCallback) return obj->m_getPasswordCallback().data();

    return nullptr;
}

rfbCredential* VNC::getCredentialProc(rfbClient* c, int credentialType)
{
    auto obj = VNC::instance(c);
    if (obj && obj->m_getCredentialCallback) return obj->m_getCredentialCallback(credentialType);

    return nullptr;
}

rfbBool VNC::mallocFrameBufferProc(rfbClient* c)
{
    auto obj = VNC::instance(c);
    if (obj && obj->m_mallocFrameBufferCallback) return obj->m_mallocFrameBufferCallback();
    return false;
}

void VNC::gotXCutTextProc(rfbClient* c, const char *text, int textlen)
{
    auto obj = VNC::instance(c);
    if (obj && obj->m_gotXCutTextCallback) obj->m_gotXCutTextCallback(text, textlen);
}

void VNC::bellProc(rfbClient* c)
{
    auto obj = VNC::instance(c);
    if (obj && obj->m_bellCallback) obj->m_bellCallback();
}

void VNC::gotCursorShapeProc(rfbClient* c, int xhot, int yhot, int width, int height, int bytesPerPixel)
{
    auto obj = VNC::instance(c);
    if (obj && obj->m_gotCursorShapeCallback) obj->m_gotCursorShapeCallback(xhot, yhot, width, height, bytesPerPixel);
}

void VNC::gotCopyRectProc(rfbClient* c, int src_x, int src_y, int w, int h, int dest_x, int dest_y)
{
    auto obj = VNC::instance(c);
    if (obj && obj->m_gotCopyRectCallback) obj->m_gotCopyRectCallback(src_x, src_y, w, h, dest_x, dest_y);
}

VNC::VNC()
    :m_rfbClient( rfbGetClient(BITS_PER_SAMPLE, SAMPLES_PER_PIXEL, BYTES_PER_PIXEL) )
    , m_connected(false)
{
    m_instances.insert({m_rfbClient, this});

    m_rfbClient->HandleTextChat = &handleTextChatProc;
    m_rfbClient->HandleKeyboardLedState = &handleKeyboardLedStateProc;
    m_rfbClient->HandleCursorPos = &handleCursorPosProc;
    m_rfbClient->SoftCursorLockArea = &softCursorLockAreaProc;
    m_rfbClient->SoftCursorUnlockScreen = &softCursorUnlockScreenProc;
    m_rfbClient->GotFrameBufferUpdate = &gotFrameBufferUpdateProc;
    m_rfbClient->GetPassword = &getPasswordProc;
    m_rfbClient->MallocFrameBuffer = &mallocFrameBufferProc;
    m_rfbClient->GotXCutText = &gotXCutTextProc;
    m_rfbClient->Bell = &bellProc;
    m_rfbClient->GotCursorShape = &gotCursorShapeProc;
    m_rfbClient->GotCopyRect = &gotCopyRectProc;
    m_rfbClient->GetCredential = &getCredentialProc;
    m_rfbClient->HandleXvpMsg = &handleXvpMsgProc;
    m_rfbClient->FinishedFrameBufferUpdate = &finishedFrameBufferUpdateProc;

    //m_rfbClient->programName = QVNCVIEWER_APP_TITLE_CSTR;
    m_rfbClient->frameBuffer = nullptr;
    m_rfbClient->canHandleNewFBSize = TRUE;
    m_rfbClient->canUseCoRRE = TRUE;
    m_rfbClient->canUseHextile = TRUE;

    m_rfbClient->appData.requestedDepth = 0;
    m_rfbClient->appData.forceTrueColour = TRUE;
    m_rfbClient->appData.useRemoteCursor = false;
    m_rfbClient->appData.enableJPEG = TRUE;
    m_rfbClient->appData.compressLevel = 0;
    m_rfbClient->appData.qualityLevel = 9;
    m_rfbClient->appData.encodingsString = "ultra";
}

VNC::~VNC()
{
    m_instances.erase( m_rfbClient );
    disconnect();
}

void VNC::connect(const std::string& host, uint16_t port)
{
    try {
        if(!ConnectToRFBServer(m_rfbClient, host.c_str(), port))
            throw "unable to connect";

        if(!InitialiseRFBConnection(m_rfbClient))
            throw "initialization failed";

        //m_rfbClient->appData.encodingsString = "ultra";
        //m_rfbClient->appData.qualityLevel = 5;
        //m_rfbClient->appData.compressLevel = 9;

        if(!SetFormatAndEncodings(m_rfbClient))
            throw "setting encoding failed";

        m_rfbClient->width = m_rfbClient->si.framebufferWidth;
        m_rfbClient->height = m_rfbClient->si.framebufferHeight;
        m_rfbClient->frameBuffer = (uint8_t *)malloc(m_rfbClient->width * m_rfbClient->height * BYTES_PER_PIXEL);

        m_connected = true;
        if (m_connectedCallback) m_connectedCallback();
    } catch(const char* err) {
        disconnect();
    }
}

void VNC::disconnect()
{
    m_connected = false;
    if (m_disconnectedCallback) m_disconnectedCallback();

    if(m_rfbClient && m_rfbClient->sock) {
        ::close(m_rfbClient->sock);
        m_rfbClient->sock = 0;
    }

    if(m_rfbClient && m_rfbClient->frameBuffer) {
        ::free(m_rfbClient->frameBuffer);
        m_rfbClient->frameBuffer = nullptr;
    }

    ::rfbClientCleanup( m_rfbClient );

}


VNC* VNC::instance(rfbClient* c)
{
    auto obj = m_instances.find(c);
    if ( obj != m_instances.end() ){
        return obj->second;
    }

    return nullptr;
}

void VNC::setHandleTextChatCallback(const HandleTextChatCallback &newHandleTextChatCallback)
{
    m_handleTextChatCallback = newHandleTextChatCallback;
}

void VNC::setHandleXvpMsgCallback(const HandleXvpMsgCallback &newHandleXvpMsgCallback)
{
    m_handleXvpMsgCallback = newHandleXvpMsgCallback;
}

void VNC::setHandleKeyboardLedStateCallback(const HandleKeyboardLedStateCallback &newHandleKeyboardLedStateCallback)
{
    m_handleKeyboardLedStateCallback = newHandleKeyboardLedStateCallback;
}

void VNC::setHandleCursorPosCallback(const HandleCursorPosCallback &newHandleCursorPosCallback)
{
    m_handleCursorPosCallback = newHandleCursorPosCallback;
}

void VNC::setSoftCursorLockAreaCallback(const SoftCursorLockAreaCallback &newSoftCursorLockAreaCallback)
{
    m_softCursorLockAreaCallback = newSoftCursorLockAreaCallback;
}

void VNC::setSoftCursorUnlockScreenCallback(const SoftCursorUnlockScreenCallback &newSoftCursorUnlockScreenCallback)
{
    m_softCursorUnlockScreenCallback = newSoftCursorUnlockScreenCallback;
}

void VNC::setGotFrameBufferUpdateCallback(const GotFrameBufferUpdateCallback &newGotFrameBufferUpdateCallback)
{
    m_gotFrameBufferUpdateCallback = newGotFrameBufferUpdateCallback;
}

void VNC::setFinishedFrameBufferUpdateCallback(const FinishedFrameBufferUpdateCallback &newFinishedFrameBufferUpdateCallback)
{
    m_finishedFrameBufferUpdateCallback = newFinishedFrameBufferUpdateCallback;
}

void VNC::setGetPasswordCallback(const GetPasswordCallback &newGetPasswordCallback)
{
    m_getPasswordCallback = newGetPasswordCallback;
}

void VNC::setGetCredentialCallback(const GetCredentialCallback &newGetCredentialCallback)
{
    m_getCredentialCallback = newGetCredentialCallback;
}

void VNC::setMallocFrameBufferCallback(const MallocFrameBufferCallback &newMallocFrameBufferCallback)
{
    m_mallocFrameBufferCallback = newMallocFrameBufferCallback;
}

void VNC::setGotXCutTextCallback(const GotXCutTextCallback &newGotXCutTextCallback)
{
    m_gotXCutTextCallback = newGotXCutTextCallback;
}

void VNC::setBellCallback(const BellCallback &newBellCallback)
{
    m_bellCallback = newBellCallback;
}

void VNC::setGotCursorShapeCallback(const GotCursorShapeCallback &newGotCursorShapeCallback)
{
    m_gotCursorShapeCallback = newGotCursorShapeCallback;
}

void VNC::setGotCopyRectCallback(const GotCopyRectCallback &newGotCopyRectCallback)
{
    m_gotCopyRectCallback = newGotCopyRectCallback;
}

rfbClient *VNC::client() const
{
    return m_rfbClient;
}

bool VNC::isConnected()
{
    return m_connected;
}

void VNC::requestUpdate()
{
    m_rfbClient->updateRect.x = m_rfbClient->updateRect.y = 0;
    m_rfbClient->updateRect.w = m_rfbClient->width;
    m_rfbClient->updateRect.h = m_rfbClient->height;
    ::SendIncrementalFramebufferUpdateRequest(m_rfbClient);
    //SendFramebufferUpdateRequest(m_rfbClient, 0, 0, m_rfbClient->width, m_rfbClient->height, true);
}

void VNC::waitResponse()
{
    if( ::WaitForMessage( m_rfbClient, 0 )) {
        if( ::HandleRFBServerMessage( m_rfbClient )) {
        }
    }
}

void VNC::cursorMoveTo(int x, int y)
{
    if (m_connected && ! ::SendPointerEvent(m_rfbClient, x , y, 0) ) {
        throw "error SendPointerEvent";
    }
}

void VNC::keyPress(uint32_t keysum, bool isPress)
{
    if (m_connected) {
        ::SendKeyEvent(m_rfbClient, keysum, isPress );
    }
}

} // namespace RVisCtl
