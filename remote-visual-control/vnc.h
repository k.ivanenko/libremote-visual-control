#ifndef PROTOCOL_VNC_H
#define PROTOCOL_VNC_H

#include <functional>
#include <map>
#include <string>

#include <rfb/rfbclient.h>

#include <remote-visual-control/i-protocol.h>

#define BITS_PER_SAMPLE   8
#define SAMPLES_PER_PIXEL 1
#define BYTES_PER_PIXEL   4

namespace RVisCtl {

class VNC : public IProtocol
{
public:
    VNC();
    ~VNC();

    // IProtocol interface
    void connect(const std::string& host, uint16_t port) override;
    void disconnect() override;
    bool isConnected() override;
    void cursorMoveTo(int x, int y) override;
    void keyPress(uint32_t keysum, bool isPress) override;

    void requestUpdate();
    void waitResponse();

    using GotFrameBufferUpdateCallback = std::function<void (int x, int y, int w, int h)>;
    using HandleTextChatCallback = std::function<void (int value, char *text)>;
    using HandleXvpMsgCallback = std::function<void (uint8_t version, uint8_t opcode)>;
    using HandleKeyboardLedStateCallback = std::function<void (int value, int pad)>;
    using HandleCursorPosCallback = std::function<bool (int x, int y)>;
    using SoftCursorLockAreaCallback = std::function<bool (int x, int y, int w, int h)>;
    using SoftCursorUnlockScreenCallback = std::function<void ()>;
    using FinishedFrameBufferUpdateCallback = std::function<void ()>;
    using GetPasswordCallback = std::function<std::string ()>;
    using GetCredentialCallback = std::function<rfbCredential* (int credentialType)>;
    using MallocFrameBufferCallback = std::function<bool ()>;
    using GotXCutTextCallback = std::function<void (const char *text, int textlen)>;
    using BellCallback = std::function<void ()>;
    using GotCursorShapeCallback = std::function<void (int xhot, int yhot, int width, int height, int bytesPerPixel)>;
    using GotCopyRectCallback = std::function<void (int src_x, int src_y, int w, int h, int dest_x, int dest_y)>;

    void setHandleTextChatCallback(const HandleTextChatCallback &callback);
    void setHandleXvpMsgCallback(const HandleXvpMsgCallback &newHandleXvpMsgCallback);
    void setHandleKeyboardLedStateCallback(const HandleKeyboardLedStateCallback &newHandleKeyboardLedStateCallback);
    void setHandleCursorPosCallback(const HandleCursorPosCallback &newHandleCursorPosCallback);
    void setSoftCursorLockAreaCallback(const SoftCursorLockAreaCallback &newSoftCursorLockAreaCallback);
    void setSoftCursorUnlockScreenCallback(const SoftCursorUnlockScreenCallback &newSoftCursorUnlockScreenCallback);
    void setGotFrameBufferUpdateCallback(const GotFrameBufferUpdateCallback &newGotFrameBufferUpdateCallback);
    void setFinishedFrameBufferUpdateCallback(const FinishedFrameBufferUpdateCallback &newFinishedFrameBufferUpdateCallback);
    void setGetPasswordCallback(const GetPasswordCallback &newGetPasswordCallback);
    void setGetCredentialCallback(const GetCredentialCallback &newGetCredentialCallback);
    void setMallocFrameBufferCallback(const MallocFrameBufferCallback &newMallocFrameBufferCallback);
    void setGotXCutTextCallback(const GotXCutTextCallback &newGotXCutTextCallback);
    void setBellCallback(const BellCallback &newBellCallback);
    void setGotCursorShapeCallback(const GotCursorShapeCallback &newGotCursorShapeCallback);
    void setGotCopyRectCallback(const GotCopyRectCallback &newGotCopyRectCallback);

    rfbClient* client() const;

private:
    rfbClient* m_rfbClient;
    bool m_connected;

    static std::map<rfbClient*, VNC*> m_instances;
    static VNC* instance(rfbClient* c);

    static void handleTextChatProc(rfbClient* c, int value, char *text);
    HandleTextChatCallback m_handleTextChatCallback;

    static void handleXvpMsgProc(rfbClient* c, uint8_t version, uint8_t opcode);
    HandleXvpMsgCallback m_handleXvpMsgCallback;

    static void handleKeyboardLedStateProc(rfbClient* c, int value, int pad);
    HandleKeyboardLedStateCallback m_handleKeyboardLedStateCallback;

    static rfbBool handleCursorPosProc(rfbClient* c, int x, int y);
    HandleCursorPosCallback m_handleCursorPosCallback;

    static void softCursorLockAreaProc(rfbClient* c, int x, int y, int w, int h);
    SoftCursorLockAreaCallback m_softCursorLockAreaCallback;

    static void softCursorUnlockScreenProc(rfbClient* c);
    SoftCursorUnlockScreenCallback m_softCursorUnlockScreenCallback;

    static void gotFrameBufferUpdateProc(rfbClient* c, int x, int y, int w, int h);
    GotFrameBufferUpdateCallback m_gotFrameBufferUpdateCallback;

    static void finishedFrameBufferUpdateProc(rfbClient* c);
    FinishedFrameBufferUpdateCallback m_finishedFrameBufferUpdateCallback;

    static char* getPasswordProc(rfbClient* c);
    GetPasswordCallback m_getPasswordCallback;

    static rfbCredential* getCredentialProc(rfbClient* c, int credentialType);
    GetCredentialCallback m_getCredentialCallback;

    static rfbBool mallocFrameBufferProc(rfbClient* c);
    MallocFrameBufferCallback m_mallocFrameBufferCallback;

    static void gotXCutTextProc(rfbClient* c, const char *text, int textlen);
    GotXCutTextCallback m_gotXCutTextCallback;

    static void bellProc(rfbClient* c);
    BellCallback m_bellCallback;

    static void gotCursorShapeProc(rfbClient* c, int xhot, int yhot, int width, int height, int bytesPerPixel);
    GotCursorShapeCallback m_gotCursorShapeCallback;

    static void gotCopyRectProc(rfbClient* c, int src_x, int src_y, int w, int h, int dest_x, int dest_y);
    GotCopyRectCallback m_gotCopyRectCallback;

};

} // namespace Protocol

#endif // PROTOCOL_VNC_H
